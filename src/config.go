package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"
)

var Version string
var BuildTime string
var Debug string

func loadConfig() BotConfig {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	jsonConfig, err := ioutil.ReadFile(dir + "/bot_config.json")
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	var botConfig BotConfig
	if err := json.Unmarshal(jsonConfig, &botConfig); err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	botConfig.LoadedAt = time.Now()

	return botConfig
}
