package main

import "time"

type BotConfig struct {
	LoadedAt time.Time `json:"-"`
	BotToken string    `json:"token"`
	Users    []BotUser `json:"users"`
}

type BotUser struct {
	Login    string    `json:"login"`
	Commands []Command `json:"commands"`
}

type Command struct {
	Type int      `json:"type"`
	Name string   `json:"name"`
	Path []string `json:"path"`
}
