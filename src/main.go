package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

func initLog() *os.File {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.OpenFile(dir+"/logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	log.SetOutput(f)
	return f
}

func main() {
	fmt.Println("Telegram bot runing")
	fmt.Printf("version: %s\n", Version)
	fmt.Printf("debug: %s\n", Debug)
	fmt.Printf("build time: %s\n", BuildTime)

	file := initLog()
	defer file.Close()

	botConfig := loadConfig()

	runBot(botConfig)
}
