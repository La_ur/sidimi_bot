package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func runBot(botConfig BotConfig) {
	bot, err := tgbotapi.NewBotAPI(botConfig.BotToken)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = Debug == "true"

	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	var msgText tgbotapi.MessageConfig
	var msgPhoto tgbotapi.PhotoConfig

BOT_LOOP:
	for update := range updates {
		if update.Message == nil {
			continue
		}
		if update.Message.From.IsBot {
			continue
		}

		for _, user := range botConfig.Users {
			if user.Login == update.Message.From.UserName {
				keyboard := tgbotapi.NewReplyKeyboard()
				for _, command := range user.Commands {
					keyboard.Keyboard = append(keyboard.Keyboard,
						tgbotapi.NewKeyboardButtonRow(
							tgbotapi.NewKeyboardButton(command.Name),
						))
				}

				for _, command := range user.Commands {
					if command.Name == update.Message.Text {
						switch command.Type {
						case 0:
							msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Подождите загрузки всех фото")
							msgText.ReplyMarkup = keyboard

							_, err = bot.Send(msgText)
							if err != nil {
								log.Println(err)
								continue BOT_LOOP
							}

							for _, url := range command.Path {
								res, err := http.Get(url)

								if err != nil {
									log.Println(err)
									msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Что-то не так")
									msgText.ReplyMarkup = keyboard

									_, err = bot.Send(msgText)
									if err != nil {
										log.Println(err)
										continue BOT_LOOP
									}
									continue BOT_LOOP
								}

								content, err := ioutil.ReadAll(res.Body)

								if err != nil {
									log.Println(err)
									msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Что-то не так")
									msgText.ReplyMarkup = keyboard

									_, err = bot.Send(msgText)
									if err != nil {
										log.Println(err)
										continue BOT_LOOP
									}
									continue BOT_LOOP
								}

								bytes := tgbotapi.FileBytes{Name: "image.jpg", Bytes: content}
								msgPhoto = tgbotapi.NewPhotoUpload(update.Message.Chat.ID, bytes)
								msgPhoto.ReplyMarkup = keyboard

								_, err = bot.Send(msgPhoto)
								if err != nil {
									log.Println(err)
									continue BOT_LOOP
								}
							}
							msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Все фото отправлены")
							msgText.ReplyMarkup = keyboard

							_, err = bot.Send(msgText)
							if err != nil {
								log.Println(err)
								continue BOT_LOOP
							}
							continue BOT_LOOP
						}
					}
				}
				msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Комманда не найдена")
				msgText.ReplyMarkup = keyboard

				_, err = bot.Send(msgText)
				if err != nil {
					log.Println(err)
					continue BOT_LOOP
				}
				continue BOT_LOOP
			}
		}

		msgText = tgbotapi.NewMessage(update.Message.Chat.ID, "Вы не имеете доступа к данному боту")

		_, err = bot.Send(msgText)
		if err != nil {
			log.Println(err)
			continue BOT_LOOP
		}
		continue BOT_LOOP
	}
}
