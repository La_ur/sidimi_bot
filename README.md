# Sidimi telegram bot  


## Deploy  

* [Install go-dep](https://github.com/golang/dep)  
* Run "dep install"  

## Build  

We use make to build project  

There are four commands:  

* dep-ensure  
    Run dep ensure  
    
* prod-linux  
    Build bin file to $(ProjectDir)/bin/linux/ in production mode 
    
* dev-linux  
    Build bin file to $(ProjectDir)/bin/linux/ in debug mode 
    
* run-linux  
    Run bin from $(ProjectDir)/bin/linux/  

## Running  
Just run bin file  

**log is written in file "logfile" near bin file**  

You have to create file "bot_config.json" near bin file  
```json
{  
    "token":"TELEGRAM BOT TOKEN",  
    "users": [  
        {  
            "login": "TELEGRAM LOGIN",  
            "commands": [  
                {  
                    "type":0,  
                    "name":"Button Text",  
                    "path":[  
                        "URL",  
                        "URL"  
                    ]  
                }  
            ]  
        }  
    ]  
}  
```  